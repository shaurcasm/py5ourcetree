from matplotlib import pyplot
import pyfirmata
from time import sleep

# Associate port and board with pyFirmata
port = 'COM3'
board = pyfirmata.Arduino(port)
sleep(1)

# Using iterator thread to avoid buffer overflow
it = pyfirmata.util.Iterator(board)
it.start()

# Assign a role and variable to analog pin 0 
a0 = board.get_pin('a:0:i')

pyplot.ion()

pData = [None] * 25
fig = pyplot.figure()
pyplot.title('Real-time Potentiometer reading')
ax1 = pyplot.axes()
l1, = pyplot.plot(pData)
pyplot.ylim([0, 1])

while True:
    try:
        sleep(1)
        pData.append(float(a0.read()))
        del pData[0]
        l1.set_xdata([i for i in xrange(25)])
        l1.set_ydata(pData)  # update the data
        pyplot.draw()  # update the plot
    except KeyboardInterrupt:
        board.exit()
        break
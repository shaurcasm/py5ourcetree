import Tkinter
import pyfirmata
from time import sleep
import os

def onStartButtonPress():
	timePeriod = timePeriodEntry.get()
	timePeriod = float(timePeriod)
	ledBrightness = brightnessScale.get()
	ledBrightness = float(ledBrightness)
	startButton.config(state = Tkinter.DISABLED)
	ledPin.write(ledBrightness/100.0)
	sleep(timePeriod)
	ledPin.write(0)
	startButton.config(state = Tkinter.ACTIVE)

port = 'COM3'
board = pyfirmata.Arduino(port)
sleep(5)

ledPin = board.get_pin('d:11:p')

top = Tkinter.Tk()
top.title("Specify light intensity using Scale")
top.minsize(300,30)

timePeriodEntry = Tkinter.Entry(top, bd = 5, width = 25)
timePeriodEntry.pack()
timePeriodEntry.focus_set()

brightnessScale = Tkinter.Scale(top, from_ = 0, to = 100, orient = Tkinter.HORIZONTAL)
brightnessScale.pack()

startButton = Tkinter.Button(top, text = "Start", command = onStartButtonPress)
startButton.pack()

#Activate Window
top.mainloop()

board.exit()
os._exit()
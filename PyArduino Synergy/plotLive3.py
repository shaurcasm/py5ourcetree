from matplotlib import pyplot
import matplotlib.animation as animation
import pyfirmata
from time import sleep

# Associate port and board with pyFirmata
port = 'COM3'
board = pyfirmata.Arduino(port)
sleep(1)

# Using iterator thread to avoid buffer overflow
it = pyfirmata.util.Iterator(board)
it.start()

# Assign a role and variable to analog pin 0
a0 = board.get_pin('a:0:i')

n = 30
pData = [None] * n

fig, ax = pyplot.subplots()
pyplot.title('Real-time Potentiometer reading')
l1, = ax.plot(pData)
# Display past sampling times as negative, with 0 meaning "now"
l1.set_xdata(range(-n + 1, 1))
ax.set(ylim=(0, 1), xlim=(-n + 1, 0))

def update(data):
    del pData[0]
    pData.append(float(a0.read()))
    l1.set_ydata(pData)  # update the data
    return l1,

ani = animation.FuncAnimation(fig, update, interval=1000, blit=True)

try:
    pyplot.show()
finally:
    pass
    board.exit()

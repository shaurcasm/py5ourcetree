from pyfirmata import Arduino, util
from time import sleep
import os

port = 'COM3'
board = Arduino(port)
sleep(5)

it = util.Iterator(board)
it.start()

def dcMotorControl (r, deltaT):
	pwmPin.write(r)
	sleep(deltaT)
	pwmPin.write(0)

# Set mode of pin 3 as PWM
pwmPin = board.get_pin('d:3:p')

a0 = board.get_pin('a:0:i')

try:
	while True:
		r = a0.read()
		# r = input("Enter value to set motor speed: ")
		# if (r>1) or (r<0):
		#	print "Enter appropriate value."
		#	board.exit()
		#	break
		print r
		t = input("How long? (Seconds)")
		dcMotorControl (r, t)
except KeyboardInterrupt:
	board.exit()
	os._exit()   
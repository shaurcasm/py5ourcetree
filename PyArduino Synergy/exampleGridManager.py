import Tkinter
import pyfirmata
from time import sleep
import os

def onStartButtonPress():
	timePeriod = timePeriodEntry.get()
	timePeriod = float(timePeriod)
	ledBrightness = brightnessScale.get()
	ledBrightness = float(ledBrightness)
	startButton.config(state = Tkinter.DISABLED)
	ledPin.write(ledBrightness/100.0)
	sleep(timePeriod)
	ledPin.write(0)
	startButton.config(state = Tkinter.ACTIVE)

port = 'COM3'
board = pyfirmata.Arduino(port)
sleep(5)

ledPin = board.get_pin('d:11:p')

top = Tkinter.Tk()
top.title("Specify light intensity using Scale")
top.minsize(300,30)

timePeriodEntry = Tkinter.Entry(top, bd = 5)
timePeriodEntry.grid(column = 1, row = 1)
timePeriodEntry.focus_set()
Tkinter.Label(top, text = "Time (in Seconds)").grid(column = 2, row = 1)

brightnessScale = Tkinter.Scale(top, from_ = 0, to = 100, orient = Tkinter.HORIZONTAL)
brightnessScale.grid(column = 1, row = 2)
Tkinter.Label(top, text = "Brightness (%)").grid(column = 2, row = 2)

startButton = Tkinter.Button(top, text = "Start", command = onStartButtonPress)
startButton.grid(column = 1, row = 3)

exitButton = Tkinter.Button(top, text = "Exit", command = top.quit)
exitButton.grid(column = 2, row = 3)

#Activate Window
top.mainloop()

board.exit()
os._exit()
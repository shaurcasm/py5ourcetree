import Tkinter
import pyfirmata
from time import sleep
import os

def onStartButtonPress():
	greenPin.write(greenVar.get())
	yellowPin.write(yellowVar.get())

def onStopButtonPress():
	greenPin.write(0)
	yellowPin.write(0)

port = 'COM3'
board = pyfirmata.Arduino(port)
sleep(5)

greenPin = board.get_pin('d:10:o')
yellowPin = board.get_pin('d:11:o')

top = Tkinter.Tk()
top.title("Specify light intensity using Scale")
top.minsize(300,30)

greenVar = Tkinter.IntVar()
greenCheckBox = Tkinter.Checkbutton(top, text = "Green LED", variable = greenVar)
greenCheckBox.grid(column = 1, row = 1)

yellowVar = Tkinter.IntVar()
yellowCheckBox = Tkinter.Checkbutton(top, text = "Yellow LED", variable = yellowVar)
yellowCheckBox.grid(column = 2, row = 1)

startButton = Tkinter.Button(top, text = "Start", command = onStartButtonPress)
startButton.grid(column = 1, row = 2)

stopButton = Tkinter.Button(top, text = "Stop", command = onStopButtonPress)
stopButton.grid(column = 2, row = 2)

exitButton = Tkinter.Button(top, text = "Exit", command = top.quit)
exitButton.grid(column = 3, row = 2)

top.mainloop()
import Tkinter

#Initialise main windows with title and size
top = Tkinter.Tk()
top.title("Hellow GUI!")
top.minsize(200,30)

#Label widget
helloLabel = Tkinter.Label(top, text = "Hello world!")
helloLabel.pack()

# Start and open the window
top.mainloop()


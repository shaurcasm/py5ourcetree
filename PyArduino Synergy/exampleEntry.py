import Tkinter
import pyfirmata
from time import sleep

def onStartButtonPress():
	# Value for delay is obtained from the Entry widget input
	timePeriod = timePeriodEntry.get()
	timePeriod = float(timePeriod)
	startButton.config(state = Tkinter.DISABLED)
	ledPin.write(1)
	sleep(timePeriod)
	ledPin.write(0)
	startButton.config(state = Tkinter.ACTIVE)

port = 'COM3'
board = pyfirmata.Arduino(port)
sleep(5)

ledPin = board.get_pin('d:11:p')

top = Tkinter.Tk()
top.title("Specify time using Entry")
top.minsize(300,30)

timePeriodEntry = Tkinter.Entry(top, bd = 5, width = 25)
timePeriodEntry.pack()
timePeriodEntry.focus_set()
startButton = Tkinter.Button(top, text = "Start", command = onStartButtonPress)
startButton.pack()

#Activate Window
top.mainloop()
#!usr/bin/python

# Import required libraries
import pyfirmata
from time import sleep

# Define custom function to perform Blink action
def blinkLED(pin, message):
	print message
	board.digital[pin].write(1)
	sleep(1)
	board.digital[pin].write(0)	

# Define custom function to perform Object detection with sensor
def distance(TrigPin, EchoPin):
	board.digital[TrigPin].write(0)
	sleep(2/1000000.0) # Low for 2 microseconds
	board.digital[TrigPin].write(1)
	sleep(10/1000000.0) # High for 10 microseconds
	board.digital[TrigPin].write(0)
	while not EchoPin.value():
		pass
	start = time.time()
	while EchoPin.value():
		pass
	end = time.time()
	Width = end - start
	Distance = (Width/2)/29.1
	
	return Distance

# Assosciate port and board with pyfirmata
port = 'COM3'
board = pyfirmata.Arduino(port)

# Use iterator thread to avoid buffer overflow
it = pyfirmata.util.Iterator(board)
it.start()

# Define pins
TrigPin = 2
EchoPin = board.get_pin('d:3:i')
GreenLED = 10
YellowLED = 11

# Check for Ultra-sonic sensor input
while True:

	board.digital[TrigPin].write(0)
	sleep(2/1000000.0) # Low for 2 microseconds
	board.digital[TrigPin].write(1)
	sleep(10/1000000.0) # High for 10 microseconds
	board.digital[TrigPin].write(0)
	while not EchoPin.value():
		pass
	start = time.time()
	while EchoPin.value():
		pass
	end = time.time()
	Width = end - start
	value = (Width/2)/29.1
	
	while value < 0:
		pass

	if value >= 200 | value < 0:
		# Perform Blink using custom function
		blinkLED(GreenLED, "Out of range; reading invalid \n")
		sleep(1/10.0)
	elif value <=30:
		blinkLED(YellowLED, "Danger too close! \n")
		sleep(1/10.0)
	else:
		blinkLED(GreenLED, "Safe Distance. \n")
		sleep(1/10.0)

# Release the board
board.exit()	
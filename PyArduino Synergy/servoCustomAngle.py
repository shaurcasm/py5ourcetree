from pyfirmata import Arduino, SERVO
from time import sleep

# Setting up the Arduino board
port = 'COM3'
board = Arduino(port)

# Need to give some time to pyfirmata
sleep(5)

# Set mode of the pin 11 as Servo
pin = 11
board.digital[pin].mode = SERVO

# Custom angle to set Servo motor angle
def setServoAngle(pin, angle):
	board.digital[pin].write(angle)
	sleep(0.015)

# Testing the function by rotating motor in both direction
while True:
	for i in range(0, 180):
		setServoAngle(pin, i)
	for i in range(180, 1, -1):
		setServoAngle(pin, i)

	# Continue or break the testing process
	i = raw_input("Enter 'y' to continue or Enter to quit): ")
	if i == 'y':
		pass
	else:
		board.exit()
		break